import socket
import struct
import hashlib

UDP_IP = "127.0.0.1"
UDP_PORT_SERVER = 12345
UDP_PORT_CLIENT = 12346
MESSAGE = b"Hello, World!"

class Server:
    def __init__(self):
        self._clients = dict()

    def opNewClient(self, client, data):
        if client in self._clients:
            print("An old client thinks it's a new client")
        else:
            print("A new client has connected")
            self._clients[client] = Client(client)

    

    OpcodeFunctions = {
        0x01 : opNewClient
    }

    def run(self, opcode, addr, data):
        if opcode in self.OpcodeFunctions:
            self.OpcodeFunctions[opcode](self, addr, data)
        else:
            print(f"Error: unrecognized opcode {hex(opcode)}")
    

class Client:
    def __init__(self, address_info):
        self._ip_address_info = address_info
        self._key = 0

def validateMessage(message):
    return True






print("UDP target IP:", UDP_IP)
print("UDP target port:", UDP_PORT_SERVER)
print("message:", MESSAGE)

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

sock.bind((UDP_IP, UDP_PORT_SERVER))

server = Server()
while True:
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    print(f"MSG: {data.hex()} from {addr}")

    server.run(int(data[0]), addr, [])


#hashlib


    # dataarray = bytearray(data)
    # dataarray.reverse()
    # data = bytes(dataarray)
    # value = struct.unpack('f', data)[0]
    # value = value + 5.0
    # data = struct.pack('f', value)
    
    # sock.sendto(data, (UDP_IP, UDP_PORT_CLIENT))