# Notes


## Tutorials

 - https://www.youtube.com/watch?v=sQ1FpD0DYF8

## Integrity

- DIFFIE HELMAN Key exchange
 - HMAC-MD5 https://crypto.stackexchange.com/questions/9336/is-hmac-md5-considered-secure-for-authenticating-encrypted-data

## Commands/opcodes (single byte)
`Client to Server Messages`


 - `0x41 newclient/getplayer data`
   - 1 byte
 - `0x42 getstate`
   - 1 byte
- `0x43 movementfalse`
   - 2 bytes
- `0x44 movementtrue`
   - 2 bytes
   - OPCODE (1) + DIRECTION (1)
   - 9 bytes
   - OPCODE (1) + ROOM (1) + POSX (4) + POSY (4)
- `0x99 hmac`
   - 17 bytes
   - OPCODE (1) + MD5SUM (128)

`Server to Client Messages (responses)`

- `0x01 clientdata`
  - 12 bytes
  - OPCODE (1) + COLOR (3) + ROOM (1) + POSX (4) + POSY(4)