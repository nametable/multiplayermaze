import socket
import struct
import hashlib

UDP_IP = "127.0.0.1"
UDP_PORT_SERVER = 12345
UDP_PORT_CLIENT = 12346
MESSAGE = b"Hello, World!"

print("UDP target IP:", UDP_IP)
print("UDP target port:", UDP_PORT_SERVER)
print("message:", MESSAGE)

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

sock.bind((UDP_IP, UDP_PORT_CLIENT))

# while True:
#     data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
#     print(f"received message: {data.hex()} from {addr}")

#     if int(data[0]) in OpcodeFunctions:
#         OpcodeFunctions[data[0]]()


#hashlib


    # dataarray = bytearray(data)
    # dataarray.reverse()
    # data = bytes(dataarray)
    # value = struct.unpack('f', data)[0]
    # value = value + 5.0
    # data = struct.pack('f', value)
    
sock.sendto(bytes.fromhex('01'), (UDP_IP, UDP_PORT_SERVER))

sock.sendto(bytes.fromhex('55'), (UDP_IP, UDP_PORT_SERVER))