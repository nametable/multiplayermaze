extends KinematicBody2D

const Speed = 250
var MOVING_UP = false
var MOVING_DOWN = false
var MOVING_LEFT = false 
var MOVING_RIGHT = false 
var id
# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	#$'/root/Main/HUD/RichTextLabel'.set_text("X:" + str(position.x) + "\nY:" + str(position.y))  #"X:{_} Y:{_}".format([motion.x, motion.y], '{_}'))
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func process(delta):
	
	var motion = Vector2()
	
	if MOVING_UP:
		motion += Vector2(0, -1)
	if MOVING_DOWN:
		motion += Vector2(0, 1)
	if MOVING_LEFT:
		motion += Vector2(-1, 0)
	if MOVING_RIGHT:
		motion += Vector2(1, 0)
	
	motion = motion.normalized() * Speed
	

	move_and_slide(motion)
	$'/root/Main/HUD/RichTextLabel'.set_text("X:" + str(position.x) + "\nY:" + str(position.y))  #"X:{_} Y:{_}".format([motion.x, motion.y], '{_}'))


func get_player_color():
	return $ColorRect.color

func set_player_color(color):
	$ColorRect.color = color
