#	Logan Bateman
#
#
#
extends Node2D

const Constants = preload('Constants.gd')
var PlayerScene = load("res://Scenes/Player.tscn")
var KEY_UP = false
var KEY_DOWN = false
var KEY_LEFT = false
var KEY_RIGHT = false

var players = {}
var player : KinematicBody2D
var myid = null
var connected = false
var server_address = '127.0.0.1' # default
var client_port

onready var GroundMap : TileMap = $Ground
onready var WallMap : TileMap = $Walls

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var peer_udp

# Called when the node enters the scene tree for the first time.
func _ready():

	randomize()
	client_port = randi() % 20000 + 10000

	$'/root/Main/HUD/ConnectUI/btnConnect'.connect("pressed", self, "connect_button_pressed")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if peer_udp != null:
		if peer_udp.get_available_packet_count() > 0:
			print ("Packets: ", peer_udp.get_available_packet_count())
			var bytes = peer_udp.get_packet()
			var message = StreamPeerBuffer.new()
			message.set_data_array(bytes)
			var client = [peer_udp.get_packet_ip(), peer_udp.get_packet_port()]
			var message_out : StreamPeerBuffer = StreamPeerBuffer.new()

			while message.get_position() < message.get_size():
				var opcode = message.get_u8()

				print(opcode)
				# print(len(bytes))
				# print(bytes.get_string_from_ascii())
				
				match opcode:
					Constants.OPCODE_NEW_CLIENT:
						OpNewClient(client, message, message_out)
					Constants.OPCODE_GET_STATE:
						OpGetState(client, message, message_out)
					Constants.OPCODE_MOVEMENT_FALSE:
						pass
					Constants.OPCODE_MOVEMENT_TRUE:
						pass
					Constants.OPCODE_GET_PLAYER_COLOR:
						OpGetPlayerColor(client, message, message_out)
					Constants.OPCODE_GET_MAP:
						OpGetMap(client, message, message_out)
					Constants.OPCODE_GAME_WON:
						OpGameWon(client, message, message_out)

			# peer_udp.set_dest_address("127.0.0.1", client[1])
			if (len(message_out.get_data_array()) > 0):
				peer_udp.put_packet(message_out.get_data_array())

		# Player movement
		if myid != null:
			players[myid].process(delta)

func connect_button_pressed():
	server_address = $'/root/Main/HUD/ConnectUI/txtServer'.text
	peer_udp = PacketPeerUDP.new()
	peer_udp.set_dest_address(server_address, 12345)
	print("Src Port: ", client_port)
	peer_udp.listen(client_port)

	var message = StreamPeerBuffer.new()
	message.put_u8(Constants.OPCODE_NEW_CLIENT)
	peer_udp.put_packet(message.get_data_array())
	
	$HUD/ConnectUI.visible = false

func OpNewClient(client, message, message_out):
	# peer_udp.set_dest_address(client[0], client[1])

	# if clients.has(client):
	# 	pass
	# else:
	# 	clients[client] = PlayerScene.instance()
	# 	add_child(clients[client])

	# var message = StreamPeerBuffer.new()

	# message.put_u8(Constants.OPCODE_NEW_CLIENT)

	var player_id = message.get_u8()
	if not players.has(player_id):
		players[player_id] = PlayerScene.instance()
		players[player_id].id = player_id
		myid = player_id
		$HUD/InfoLabel.text = "You have joined with id " + str(myid)
		add_child(players[player_id])
		var player_cam : Camera2D = players[player_id].get_node('Camera2D')
		player_cam.current = true

	var player_color : Color = Color8(
		message.get_u8(),
		message.get_u8(),
		message.get_u8()
	)

	players[player_id].get_node('ColorRect').color = player_color

	players[player_id].position.x = message.get_float()
	players[player_id].position.y = message.get_float()

	# Request map from server
	message_out.put_u8(Constants.OPCODE_GET_MAP)

	# Request state from server
	message_out.put_u8(Constants.OPCODE_GET_STATE)

func OpGetPlayerColor(client, message, message_out):

	var player_id = message.get_u8()
	players[player_id].set_player_color(Color8(
		message.get_u8(), # R
		message.get_u8(), # G
		message.get_u8()  # B
	))
	
func OpGetMap(client, message, message_out):
	
	# # message_out.put_u8(Constants.OPCODE_GET_MAP)
	
	# var tileswalls_len = message.get_u16()
	# for index in range(tileswalls_len):
		
	# 	tilemapwalls.set_cell(
	# 		message.get_16(),
	# 		message.get_16(),
	# 		message.get_u8()

	# 	)

	# var tilesground_len = message.get_u16()
	# for index in range(tilesground_len):
	
	# 	tilemapground.set_cell(
	# 		message.get_16(),
	# 		message.get_16(),
	# 		message.get_u8()

	# 	)
	#var newwalls : TileMap = message.get_var()
	#var newground : TileMap = message.get_var()
	#tilemapwalls.replace_by(newwalls)
	#tilemapground.replace_by(newground)

	var height = message.get_u16()
	var width = message.get_u16()
	var maze_seed = message.get_64()

	seed(maze_seed)

	GroundMap.clear()
	WallMap.clear()
	# var height = 10
	# var width = 10
	var scalar = 3

	make_maze(width, height)
	GroundMap.update_bitmask_region(Vector2(0,0), Vector2(width * scalar - 1, height * scalar - 1))
	WallMap.update_bitmask_region(Vector2(0,0), Vector2(width * scalar - 1, height * scalar - 1))

	$End.position = Vector2(((width - 1) * scalar + 1) * 32, ((height - 1) * scalar + 1) * 32)

func OpGetState(client, message, message_out):
	# player_count (u8) + (player_id (u8) + x (float) + y (float)) * player_count
	var player_count = message.get_u8()

	for player_index in range(player_count):
		var player_id = message.get_u8()
		if not players.has(player_id):
			players[player_id] = PlayerScene.instance()
			players[player_id].id = player_id
			add_child(players[player_id])

			# Add a request to the server to get player color
			message_out.put_u8(Constants.OPCODE_GET_PLAYER_COLOR)
			message_out.put_u8(player_id)
		if player_id == myid:
			var x = message.get_float()
			var y = message.get_float()
			# Only reset the player position if the server and client conflict
			if abs(players[myid].position.x - x) > 80.0 or abs(players[myid].position.y - y) > 80.0:
				players[player_id].position.x = x
				players[player_id].position.y = y
		else:
			players[player_id].position.x = message.get_float()
			players[player_id].position.y = message.get_float()

func OpMovementChanged(client, message, message_out):
	pass

func OpGameWon(client, message, message_out):

	var winner_id = message.get_u8()

	if winner_id == myid:
		$HUD/InfoLabel.text = "You have won the game!"
	else:
		$HUD/InfoLabel.text = "Player with id " + str(winner_id) + " has won the game!"

	get_tree().paused = true

func _input(event):
	var message = StreamPeerBuffer.new()
	if event.is_action_pressed('player_up'):
		players[myid].MOVING_UP = true
		message.put_u8(Constants.OPCODE_MOVEMENT_TRUE)
		message.put_u8(Constants.MOVEMENT_UP)
		peer_udp.put_packet(message.get_data_array())
		# peer_udp.put_packet(var2bytes() + var2bytes(Constants.MOVEMENT_UP))
	if event.is_action_released('player_up'):
		players[myid].MOVING_UP = false
		message.put_u8(Constants.OPCODE_MOVEMENT_FALSE)
		message.put_u8(Constants.MOVEMENT_UP)
		peer_udp.put_packet(message.get_data_array())
	if event.is_action_pressed('player_down'):
		players[myid].MOVING_DOWN = true
		message.put_u8(Constants.OPCODE_MOVEMENT_TRUE)
		message.put_u8(Constants.MOVEMENT_DOWN)
		peer_udp.put_packet(message.get_data_array())
	if event.is_action_released('player_down'):
		players[myid].MOVING_DOWN = false
		message.put_u8(Constants.OPCODE_MOVEMENT_FALSE)
		message.put_u8(Constants.MOVEMENT_DOWN)
		peer_udp.put_packet(message.get_data_array())
	if event.is_action_pressed('player_left'):
		players[myid].MOVING_LEFT = true
		message.put_u8(Constants.OPCODE_MOVEMENT_TRUE)
		message.put_u8(Constants.MOVEMENT_LEFT)
		peer_udp.put_packet(message.get_data_array())
	if event.is_action_released('player_left'):
		players[myid].MOVING_LEFT = false
		message.put_u8(Constants.OPCODE_MOVEMENT_FALSE)
		message.put_u8(Constants.MOVEMENT_LEFT)
		peer_udp.put_packet(message.get_data_array())
	if event.is_action_pressed('player_right'):
		players[myid].MOVING_RIGHT = true
		message.put_u8(Constants.OPCODE_MOVEMENT_TRUE)
		message.put_u8(Constants.MOVEMENT_RIGHT)
		peer_udp.put_packet(message.get_data_array())
	if event.is_action_released('player_right'):
		players[myid].MOVING_RIGHT = false
		message.put_u8(Constants.OPCODE_MOVEMENT_FALSE)
		message.put_u8(Constants.MOVEMENT_RIGHT)
		peer_udp.put_packet(message.get_data_array())


# Maze gen adapted from http://kidscancode.org/blog/2018/08/godot3_procgen1/

const N = 1
const E = 2
const S = 4
const W = 8

var cell_walls = {Vector2(0, -1): N, Vector2(1, 0): E, 
	Vector2(0, 1): S, Vector2(-1, 0): W}

func check_neighbors(cell, unvisited):
	# returns an array of cell's unvisited neighbors
	var list = []
	for n in cell_walls.keys():
		if cell + n in unvisited:
			list.append(cell + n)
	return list
	
func make_maze(width, height):
	var unvisited = []  # array of unvisited tiles
	var stack = []
	# fill the map with solid tiles
	# Map.clear()
	for x in range(width):
		for y in range(height):
			unvisited.append(Vector2(x, y))
			set_maze_cell(Vector2(x, y), 15)
			#Map.set_cellv(Vector2(x * scalar + 1, y * scalar + 1), 0)
			#Map.set_cellv(Vector2(x, y), N|E|S|W)
	var current = Vector2(0, 0)
	unvisited.erase(current)
	# execute recursive backtracker algorithm
	while unvisited:
		var neighbors = check_neighbors(current, unvisited)
		if neighbors.size() > 0:
			var next = neighbors[randi() % neighbors.size()]
			stack.append(current)
			# remove walls from *both* cells
			var dir = next - current
			#var current_walls = Map.get_cellv(current) - cell_walls[dir]
			#var next_walls = Map.get_cellv(next) - cell_walls[-dir]
			var current_walls = get_maze_cell(current) - cell_walls[dir]
			var next_walls = get_maze_cell(next) - cell_walls[-dir]

			# Map.set_cellv(current, current_walls)
			# Map.set_cellv(next, next_walls)
			set_maze_cell(current, current_walls)
			set_maze_cell(next, next_walls)

			current = next
			unvisited.erase(current)
		elif stack:
			current = stack.pop_back()
		# yield(get_tree(), 'idle_frame')
	
# my own creation
func set_maze_cell(position, walls):
	print("X: ", position.x, " Y: ", position.y, " val: ", walls)
	if walls < 0:
		print("Settings walls to strange negative")
	var scalar = 3
	var top = (walls & N != 0)
	var right = (walls & E != 0)
	var bottom = (walls & S != 0)
	var left = (walls & W != 0)

	WallMap.set_cellv(Vector2(position.x * scalar + 1, position.y * scalar), 1 if top else -1)
	GroundMap.set_cellv(Vector2(position.x * scalar + 1, position.y * scalar), -1 if top else 0)

	WallMap.set_cellv(Vector2(position.x * scalar + 1, position.y * scalar + 2), 1 if bottom else -1)
	GroundMap.set_cellv(Vector2(position.x * scalar + 1, position.y * scalar + 2), -1 if bottom else 0)

	WallMap.set_cellv(Vector2(position.x * scalar, position.y * scalar + 1), 1 if left else -1)
	GroundMap.set_cellv(Vector2(position.x * scalar, position.y * scalar + 1), -1 if left else 0)

	WallMap.set_cellv(Vector2(position.x * scalar + 2, position.y * scalar + 1), 1 if right else -1)
	GroundMap.set_cellv(Vector2(position.x * scalar + 2, position.y * scalar + 1), -1 if right else 0)


	WallMap.set_cellv(Vector2(position.x * scalar, position.y * scalar), 1)
	WallMap.set_cellv(Vector2(position.x * scalar, position.y * scalar + 2), 1)
	WallMap.set_cellv(Vector2(position.x * scalar + 2, position.y * scalar), 1)
	WallMap.set_cellv(Vector2(position.x * scalar + 2, position.y * scalar + 2), 1)
	GroundMap.set_cellv(Vector2(position.x * scalar + 1, position.y * scalar + 1), 0)
	# GroundMap.update_bitmask_area(Vector2(position.x * scalar + 1, position.y * scalar + 1))
	# WallMap.update_bitmask_area(Vector2(position.x * scalar + 1, position.y * scalar + 1))

func get_maze_cell(position):
	var scalar = 3
	var top = WallMap.get_cellv(Vector2(position.x * scalar + 1, position.y * scalar)) == 1
	var bottom = WallMap.get_cellv(Vector2(position.x * scalar + 1, position.y * scalar + 2)) == 1
	var left = WallMap.get_cellv(Vector2(position.x * scalar, position.y * scalar + 1)) == 1
	var right = WallMap.get_cellv(Vector2(position.x * scalar + 2, position.y * scalar + 1)) == 1
	var direction = int(top) * N + int(right) * E + int(bottom) * S + int(left) * W
	if direction < 0:
		print("Wierd negative problem with maze gen")
	return direction
