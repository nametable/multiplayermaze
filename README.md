# Multiplayer Maze by Logan Bateman

 - Projects can be opened with Godot_v3.2-beta2
 - Binaries are available for Windows, Linux, and Mac
   - (no testing of Mac or Windows binaries, only linux);

 - Source code is GDScript (*.gd), a scripting language built specifically for the Godot Engine