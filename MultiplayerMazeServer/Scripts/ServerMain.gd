extends Node2D

const Constants = preload('Constants.gd')
var PlayerScene = load("res://Scenes/Player.tscn")

var peer_udp
var client_counter = 0 # sdfdsf
var clients = {}
var client_ids_by_ip = {}
var maze_seed = 0

onready var GroundMap : TileMap = $Ground
onready var WallMap : TileMap = $Walls

const maze_width = 15
const maze_height = 15

# Called when the node enters the scene tree for the first time.
func _ready():

	peer_udp = PacketPeerUDP.new()
	peer_udp.set_dest_address("127.0.0.1", 12346)
	peer_udp.listen(12345)

	# peer_udp.put_packet(var2bytes(Constants.OPCODE_NEW_CLIENT))

	# str("test").md5_buffer()
	# OpGetMap(0, StreamPeerBuffer.new())

	# get a random seed
	maze_seed = new_seed()

	GroundMap.clear()
	WallMap.clear()

	var height = maze_height
	var width = maze_width
	var scalar = 3

	seed(maze_seed)

	make_maze(width, height)
	GroundMap.update_bitmask_region(Vector2(0,0), Vector2(width * scalar - 1, height * scalar - 1))
	WallMap.update_bitmask_region(Vector2(0,0), Vector2(width * scalar - 1, height * scalar - 1))

	$End.position = Vector2(((width - 1) * scalar + 1) * 32, ((height - 1) * scalar + 1) * 32)
	$End.connect("body_entered", self, "WinState")

	$HUD/RichTextLabel.text = "Server started"

	var UpdateTimer : Timer = $UpdateTimer
	UpdateTimer.connect("timeout", self, "BroadcastUpdates")
	UpdateTimer.start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Listen for new messages
	ProcessNetworking()

	# Allow clients to move
	for client in clients.values():
		client.process(delta)

	# Send out updates
	# Time based now
	#BroadcastUpdates()


func new_seed():

	randomize()
	var random_seed = randi()
	seed(random_seed)
	return random_seed

func OpNewClient(client, message_out : StreamPeerBuffer):

	# peer_udp.set_dest_address(client[0], client[1])
	

	# Check if client exists already...
	if client_ids_by_ip.has(client) and clients.has(client_ids_by_ip[client]):
		pass
	else:
		client_ids_by_ip[client] = client_counter
		client_counter += 1
		clients[client_ids_by_ip[client]] = PlayerScene.instance()
		clients[client_ids_by_ip[client]].id = client_ids_by_ip[client]
		clients[client_ids_by_ip[client]].position = Vector2(48.0, 48.0)
		var random_color : Color = Color8(randi() % 256, randi() % 256, randi() % 256)
		clients[client_ids_by_ip[client]].set_player_color(random_color)
		add_child(clients[client_ids_by_ip[client]])

	# var message = StreamPeerBuffer.new()

	message_out.put_u8(Constants.OPCODE_NEW_CLIENT)
	message_out.put_u8(client_ids_by_ip[client]) # ID NUMBER - TEMP - TODO
	var player_color : Color = clients[client_ids_by_ip[client]].get_player_color()
	message_out.put_u8(player_color.r8) 	# R
	message_out.put_u8(player_color.g8)	# G
	message_out.put_u8(player_color.b8)	# B
	message_out.put_float(clients[client_ids_by_ip[client]].position.x)	# POS X
	message_out.put_float(clients[client_ids_by_ip[client]].position.x)	# POS Y

	
func OpGetState(client, message, message_out : StreamPeerBuffer):

	message_out.put_u8(Constants.OPCODE_GET_STATE)
	message_out.put_u8(len(clients)) # number of clients
	for player in clients.values():
		message_out.put_u8(player.id)
		message_out.put_float(player.position.x)
		message_out.put_float(player.position.y)

func OpGetPlayerColor(client, message, message_out):
	# message -> player_id (u8)

	var player_id = message.get_u8()

	if (clients.has(player_id)):
		message_out.put_u8(Constants.OPCODE_GET_PLAYER_COLOR)
		message_out.put_u8(player_id)
		var color : Color = clients[player_id].get_player_color()
		message_out.put_u8(color.r8) # R
		message_out.put_u8(color.g8) # G
		message_out.put_u8(color.b8) # B



func OpGetMap(client, message_out : StreamPeerBuffer):

	# Just send the size of the maze and the seed
	
	message_out.put_u8(Constants.OPCODE_GET_MAP)

	message_out.put_u16(maze_width)
	message_out.put_u16(maze_height)

	message_out.put_64(maze_seed)

func OpMovementChanged(client, state, message, message_out : StreamPeerBuffer):
	var movement = message.get_u8()

	if client_ids_by_ip.has(client):
		match movement:
			Constants.MOVEMENT_UP:
				clients[client_ids_by_ip[client]].MOVING_UP = state
			Constants.MOVEMENT_DOWN:
				clients[client_ids_by_ip[client]].MOVING_DOWN = state
			Constants.MOVEMENT_LEFT:
				clients[client_ids_by_ip[client]].MOVING_LEFT = state
			Constants.MOVEMENT_RIGHT:
				clients[client_ids_by_ip[client]].MOVING_RIGHT = state

			#something came

func WinState(object):
	if object is KinematicBody2D:
		$HUD/RichTextLabel.text = "Player with id " + str(object.id) + " has won!"

		var message_winner : StreamPeerBuffer = StreamPeerBuffer.new()
		message_winner.put_u8(Constants.OPCODE_GAME_WON)
		message_winner.put_u8(object.id)
	
		for client_ip in client_ids_by_ip:
			peer_udp.set_dest_address(client_ip[0], client_ip[1])
			peer_udp.put_packet(message_winner.get_data_array())

		get_tree().paused = true

func BroadcastUpdates():
	var message_updates : StreamPeerBuffer = StreamPeerBuffer.new()
	var updates = 0
	var players_with_updates = []

	# Find players that have moved
	for player in clients.values():
		if player.has_moved:
			updates += 1
			players_with_updates.append(player)

	# Only send updates if somebody moved
	if updates > 0:
		
		#print(len($End.get_overlapping_bodies()))
		
		message_updates.put_u8(Constants.OPCODE_GET_STATE)
		message_updates.put_u8(updates)

		for player in players_with_updates:
			message_updates.put_u8(player.id)
			var position : Vector2 = player.get_position_and_reset()
			message_updates.put_float(player.position.x)
			message_updates.put_float(player.position.y)
	
		for client_ip in client_ids_by_ip:
			peer_udp.set_dest_address(client_ip[0], client_ip[1])
			peer_udp.put_packet(message_updates.get_data_array())
			


func ProcessNetworking():
	if peer_udp.get_available_packet_count() > 0:
		var bytes = peer_udp.get_packet()
		var message = StreamPeerBuffer.new()
		message.set_data_array(bytes)
		var client = [peer_udp.get_packet_ip(), peer_udp.get_packet_port()]
		var message_out : StreamPeerBuffer = StreamPeerBuffer.new()

		while message.get_position() < message.get_size():
			var opcode = message.get_u8()

			#print(opcode)
			# print(len(bytes))
			# print(bytes.get_string_from_ascii())
			
			match opcode:
				Constants.OPCODE_NEW_CLIENT:
					OpNewClient(client, message_out)
				Constants.OPCODE_GET_STATE:
					OpGetState(client, message, message_out)
				Constants.OPCODE_MOVEMENT_FALSE:
					OpMovementChanged(client, false, message, message_out)
				Constants.OPCODE_MOVEMENT_TRUE:
					OpMovementChanged(client, true, message, message_out)
				Constants.OPCODE_GET_MAP:
					OpGetMap(client, message_out)

		peer_udp.set_dest_address(client[0], client[1])
		if (len(message_out.get_data_array()) > 0):
			peer_udp.put_packet(message_out.get_data_array())

# Maze gen adapted from http://kidscancode.org/blog/2018/08/godot3_procgen1/

const N = 1
const E = 2
const S = 4
const W = 8

var cell_walls = {Vector2(0, -1): N, Vector2(1, 0): E, 
	Vector2(0, 1): S, Vector2(-1, 0): W}

func check_neighbors(cell, unvisited):
	# returns an array of cell's unvisited neighbors
	var list = []
	for n in cell_walls.keys():
		if cell + n in unvisited:
			list.append(cell + n)
	return list
	
func make_maze(width, height):
	var unvisited = []  # array of unvisited tiles
	var stack = []
	# fill the map with solid tiles
	# Map.clear()
	for x in range(width):
		for y in range(height):
			unvisited.append(Vector2(x, y))
			set_maze_cell(Vector2(x, y), 15)
			#Map.set_cellv(Vector2(x * scalar + 1, y * scalar + 1), 0)
			#Map.set_cellv(Vector2(x, y), N|E|S|W)
	var current = Vector2(0, 0)
	unvisited.erase(current)
	# execute recursive backtracker algorithm
	while unvisited:
		var neighbors = check_neighbors(current, unvisited)
		if neighbors.size() > 0:
			var next = neighbors[randi() % neighbors.size()]
			stack.append(current)
			# remove walls from *both* cells
			var dir = next - current
			#var current_walls = Map.get_cellv(current) - cell_walls[dir]
			#var next_walls = Map.get_cellv(next) - cell_walls[-dir]
			var current_walls = get_maze_cell(current) - cell_walls[dir]
			var next_walls = get_maze_cell(next) - cell_walls[-dir]

			# Map.set_cellv(current, current_walls)
			# Map.set_cellv(next, next_walls)
			set_maze_cell(current, current_walls)
			set_maze_cell(next, next_walls)

			current = next
			unvisited.erase(current)
		elif stack:
			current = stack.pop_back()
		# yield(get_tree(), 'idle_frame')
	
# my own creation
func set_maze_cell(position, walls):
	#print("X: ", position.x, " Y: ", position.y, " val: ", walls)
	if walls < 0:
		print("Settings walls to strange negative")
	var scalar = 3
	var top = (walls & N != 0)
	var right = (walls & E != 0)
	var bottom = (walls & S != 0)
	var left = (walls & W != 0)

	WallMap.set_cellv(Vector2(position.x * scalar + 1, position.y * scalar), 1 if top else -1)
	GroundMap.set_cellv(Vector2(position.x * scalar + 1, position.y * scalar), -1 if top else 0)

	WallMap.set_cellv(Vector2(position.x * scalar + 1, position.y * scalar + 2), 1 if bottom else -1)
	GroundMap.set_cellv(Vector2(position.x * scalar + 1, position.y * scalar + 2), -1 if bottom else 0)

	WallMap.set_cellv(Vector2(position.x * scalar, position.y * scalar + 1), 1 if left else -1)
	GroundMap.set_cellv(Vector2(position.x * scalar, position.y * scalar + 1), -1 if left else 0)

	WallMap.set_cellv(Vector2(position.x * scalar + 2, position.y * scalar + 1), 1 if right else -1)
	GroundMap.set_cellv(Vector2(position.x * scalar + 2, position.y * scalar + 1), -1 if right else 0)


	WallMap.set_cellv(Vector2(position.x * scalar, position.y * scalar), 1)
	WallMap.set_cellv(Vector2(position.x * scalar, position.y * scalar + 2), 1)
	WallMap.set_cellv(Vector2(position.x * scalar + 2, position.y * scalar), 1)
	WallMap.set_cellv(Vector2(position.x * scalar + 2, position.y * scalar + 2), 1)
	GroundMap.set_cellv(Vector2(position.x * scalar + 1, position.y * scalar + 1), 0)
	# GroundMap.update_bitmask_area(Vector2(position.x * scalar + 1, position.y * scalar + 1))
	# WallMap.update_bitmask_area(Vector2(position.x * scalar + 1, position.y * scalar + 1))

func get_maze_cell(position):
	var scalar = 3
	var top = WallMap.get_cellv(Vector2(position.x * scalar + 1, position.y * scalar)) == 1
	var bottom = WallMap.get_cellv(Vector2(position.x * scalar + 1, position.y * scalar + 2)) == 1
	var left = WallMap.get_cellv(Vector2(position.x * scalar, position.y * scalar + 1)) == 1
	var right = WallMap.get_cellv(Vector2(position.x * scalar + 2, position.y * scalar + 1)) == 1
	var direction = int(top) * N + int(right) * E + int(bottom) * S + int(left) * W
	if direction < 0:
		print("Wierd negative problem with maze gen")
	return direction
