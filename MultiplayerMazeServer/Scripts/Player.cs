using Godot;
using System;
using System.Text;

public class Player : KinematicBody2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	public int Speed = 125;
	private Vector2 _velocity = new Vector2();

	const float movement_per_tick = 5.0f;

	PacketPeerUDP peer_udp;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		peer_udp = new PacketPeerUDP();
		peer_udp.SetDestAddress("127.0.0.1", 12345);
		peer_udp.Listen(12346);
		Console.WriteLine("Started !");
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		_velocity = new Vector2();

		// if (peer_udp.GetAvailablePacketCount() > 0) {
		// 	Vector2 cur_position = this.GetPosition();
		// 	while(peer_udp.GetAvailablePacketCount() > 0) {
		// 		//received_pos = (Vector2)peer_udp.GetVar(true);
		// 		byte[] BytesReceived = peer_udp.GetPacket();
		// 		// if (BitConverter.IsLittleEndian)
		// 		// 	Array.Reverse(BytesReceived);
		// 		cur_position.x = (float)BitConverter.ToSingle(BytesReceived, 0);
		// 		//object test = peer_udp.GetVar(true);
		// 		// Console.WriteLine(test.ToString());
		// 	}
			
			
		// 	this.SetPosition(cur_position);
		// }


		if (Input.IsActionPressed("player_right"))
		{
			// byte[] bytes = Encoding.ASCII.GetBytes("test");
			//byte[] bytes;

			
			
			// Move right
			_velocity.x += 1;
			//this.SetPosition(this.GetPosition() + new Vector2(movement_per_tick, 0.0f));
			
			// Vector2 cur_position = this.GetPosition();
			// byte[] floatBytes = BitConverter.GetBytes(cur_position.x);
			// if (BitConverter.IsLittleEndian)
			// 	Array.Reverse(floatBytes);
			// byte[] bytes = floatBytes;
			// //peer_udp.PutVar(cur_position);
			// peer_udp.PutPacket(bytes);
			//this.MoveLocalX(delta, false);
		} 
		if (Input.IsActionPressed("player_left")) {
			_velocity.x -= 1;
			//this.SetPosition(this.GetPosition() + new Vector2(-movement_per_tick, 0.0f));
		} 
		if (Input.IsActionPressed("player_up")) {
			_velocity.y -= 1;
			// this.SetPosition(this.GetPosition() + new Vector2(0.0f, -movement_per_tick));
		} 
		if (Input.IsActionPressed("player_down")) {
			_velocity.y += 1;
			// this.SetPosition(this.GetPosition() + new Vector2(0.0f, movement_per_tick));
		}
		MoveAndSlide(_velocity * Speed);
	}
}
